const Post = require("../../models/Post");
const { isEmpty } = require("lodash");

exports.create = (req, res) => {
  if (!req.body.title || !req.body.description) {
    res
      .status(400)
      .json({ resultcode: 1, message: "Content can not be empty!" });
    return;
  }

  const newPost = new Post({
    title: req.body.title,
    description: req.body.description,
    image: req.body.image,
  });

  newPost
    .save()
    .then((data) => {
      res.status(201).json({ resultcode: 0, data });
    })
    .catch((err) => {
      res.status(500).json({
        resultcode: 1,
        message:
          err.message || "Some error occurred while creating the new post.",
      });
    });
};

exports.findAll = (req, res) => {
  Post.find()
    .then((data) => res.status(200).json({ resultcode: 0, data }))
    .catch((err) => {
      res.status(500).json({
        resultcode: 1,
        message: err.message || "Some error occurred while retrieving posts.",
      });
    });
};

exports.findById = (req, res) => {
  const id = req.params.id;
  Post.findById(id)
    .then((data) => {
      if (!data) {
        res
          .status(404)
          .json({ resultcode: 1, message: "Not found post with id " + id });
      } else {
        res.status(200).json({ resultcode: 0, data });
      }
    })
    .catch((err) => {
      res.status(500).json({
        resultcode: 1,
        message: "Error retrieving post with id " + id,
      });
    });
};

exports.update = (req, res) => {
  const {
    params: { id },
    body,
  } = req;
  if (isEmpty(body)) {
    return res.status(400).json({
      resultcode: 1,
      message: "Content to update can not be empty!",
    });
  } else {
    Post.findByIdAndUpdate(id, body)
      .then((data) => {
        if (!data) {
          res.status(404).json({
            resultcode: 1,
            message: `Cannot update post with id ${id}!`,
          });
        }
        res.status(201).json({
          resultcode: 0,
          message: "Post updated!",
          post: data,
        });
      })
      .catch((err) => {
        res.status(500).json({
          resultcode: 1,
          message: "Error updating post with id " + id,
        });
      });
  }
};

exports.remove = (req, res) => {
  const id = req.params.id;
  Post.findByIdAndRemove(id).then((data) => {
    if (!data) {
      res.status(404).json({
        resultcode: 1,
        message: `Cannot delete post with id ${id}.`,
      });
      return;
    }
    res.status(200).json({
      resultcode: 0,
      message: "Post deleted!",
      post: data,
    });
  });
};

exports.removeAll = (req, res) => {
  Post.deleteMany({})
    .then((data) => {
      res.status(200).json({
        resultcode: 0,
        message: `${data.deletedCount} posts were deleted successfully!`,
      });
    })
    .catch((err) => {
      res.status(500).json({
        resultcode: 1,
        message: err.message || "Some error occurred while removing all posts.",
      });
    });
};
