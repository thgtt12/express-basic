const { Router } = require("express");
const {
  create,
  findAll,
  findById,
  update,
  remove,
  removeAll,
} = require("../controllers/posts/index");
const { login, register } = require("../controllers/users/index");
const auth = require("../util/check-auth");

const router = Router();

router.post("/login", login);

router.post("/register", register);

router.get("/get-posts", findAll);

router.get("/post/:id", findById);

router.post("/create-post", auth, create);

router.put("/edit-post/:id", auth, update);

router.delete("/delete-post/:id", auth, remove);

router.delete("/delete-all", auth, removeAll);

module.exports = router;
